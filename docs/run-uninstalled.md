
# Running tuxpkg uninstalled


!!! note
    tuxpkg requires Python 3.6 or newer.

If you don't want to or can't install tuxpkg, you can run it directly from the
source directory. 

```shell
python3 -m /path/to/tuxpkg/tuxpkg --help
sudo ln -s /path/to/tuxpkg/tuxpkg /usr/local/bin/tuxpkg && tuxpkg --help
```
