
## Installing from PyPI


!!! note
    tuxpkg requires Python 3.6 or newer.

To install tuxpkg on your system globally:

```shell
sudo pip3 install -U tuxpkg
```

To install tuxpkg to your home directory at ~/.local/bin:

```shell
pip3 install -U --user tuxpkg
```

To upgrade tuxpkg to the latest version, run the same command you ran to
install it.
